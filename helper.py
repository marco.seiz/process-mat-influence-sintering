import re
import numpy as np
import warnings
import sys

# scales employed
l0 = 1e-8  # m
D0 = 1e-12  # m^2/s
t0 = l0**2 / D0  # s
T0 = 700  # K
Es0 = 1  # J/m^2
Er0 = Es0 / l0  # J/m^3
Vm0 = 7.1e-6  # m^3/mol
E0 = Er0 * Vm0  # J/mol
glob_regexen = {}
glob_handlers = {}


# input is cells*dx !
def physSizeNm(length):
    return length * l0 / 1e-9


def mu2MPa(mu):
    return mu * Er0 / 1e6


def vol2r(vol):
    return (3 * vol / (4 * np.pi)) ** (1.0 / 3.0)


def calcInvariants(m, v):
    Om1 = m[:, 0, 0] + m[:, 1, 1] + m[:, 2, 2]
    Om2 = (
        m[:, 0, 0] * m[:, 1, 1]
        + m[:, 0, 0] * m[:, 2, 2]
        + m[:, 1, 1] * m[:, 2, 2]
        - m[:, 0, 1] ** 2
        - m[:, 0, 2] ** 2
        - m[:, 1, 2] ** 2
    )
    Om3 = (
        m[:, 0, 0] * m[:, 1, 1] * m[:, 2, 2]
        + 2 * m[:, 0, 1] * m[:, 0, 2] * m[:, 1, 2]
        - m[:, 0, 0] * m[:, 1, 2] ** 2
        - m[:, 1, 1] * m[:, 0, 2] ** 2
        - m[:, 2, 2] * m[:, 0, 1] ** 2
    )
    # should be v.shape[0] x [3]
    return np.array(
        [3 * v ** (5.0 / 3.0) / Om1, 3 * v ** (10.0 / 3.0) / Om2, v**5 / Om3]
    )


Oms_ref = {
    "cube": np.array([12, 144, 1728]),
    "sphere": np.array(
        [
            (2000 * np.pi**2 / 9) ** (1.0 / 3.0),
            (2000 * np.pi**2 / 9) ** (2.0 / 3.0),
            (2000 * np.pi**2 / 9) ** (3.0 / 3.0),
        ]
    ),
    "trunc_octa": np.array(
        [12.73183377, 162.09959108, 2063.82504738]
    ),  # calc'd with latte-integrale
}


def testInvariants(shape, invs):
    diffs = Oms_ref[shape] - invs
    reldiffs = diffs / (Oms_ref[shape])
    print(reldiffs)
    if np.sum(reldiffs**2) < 1e-3:
        print("should be alright")
        return False
    return True


def filteredInvariantCalc(mom, vol, filt):
    volfilt = vol[filt]
    # momfilt = mom[filt]
    if len(mom.shape) == 2:
        invs = calcInvariants(
            mom.reshape(1, 3, 3)[filt], volfilt
        )  # actually none, but ehh
    else:
        invs = calcInvariants(mom[filt], volfilt)
    return invs, volfilt  # to append or w/e


# handlers take a simulation name string
# and return a part to build a descriptive label for plots
# their output can also be used to identify simulations
# handlers should always take a string, an optional warn flag and an optional pprint (pretty printer)
# if pprint=None is passed, it should return something which is easily comparable (string, int, NOT float) since that goes into meta info

rer = re.compile("_r([0-9]+)[_-]")  # yes
redx = re.compile("dx([0-9\.]+)[_-]")
# no defaults!
# pprint does m -> nm
def rHandler(
    s,
    warn=False,
    pprint=lambda x: "R = %.2g nm" % (physSizeNm(x)),
    nodefaultprint=False,
):
    """
    nodefaultprint currently ignored
    """
    m = redx.search(s)
    if m is not None:
        dx = float(m.groups()[0])
    else:
        raise Exception("no dx found in name %s" % (s))
    m = rer.search(s)
    if m is not None:
        r = float(m.groups()[0])
    else:
        raise Exception("no r found in name %s" % (s))
    R = dx * r
    if pprint is not None:
        R = pprint(R)
    else:
        R = "%.3g" % (R)
    return R


glob_handlers["R"] = rHandler


repack = re.compile("pack([0-9]+)_")
# no defaults!
# i guess this could be handled by floatHandler actually? except for the int() part but that might be just a move to a generic numberHandlerGenerator or w/e
def packHandler(s, warn=False, pprint=lambda x: r"$%d^3$" % (x)):
    m = repack.search(s)
    baselen = 0
    if m is not None:
        baselen = int(m.groups()[0])
    else:
        raise Exception("no pack found in name %s" % (s))
    if pprint is not None:
        baselen = pprint(baselen)
    return baselen  # keep voxel count for now


glob_handlers["pack"] = packHandler


rewangtype = re.compile("-wang-(.+)")

# this is quite disgusting but works
def modelHandler(s, warn=False, pprint=None):
    model = ""
    if "noadv" in s or "DO" in s:
        model = "DO"
        return model
    m = rewangtype.search(s)
    if m is not None:
        wangtype = str(m.groups()[0])
        if "c0" in wangtype:
            wangtype = "C"
        elif "musurf" in wangtype:
            wangtype = "$\mu$"
        elif "muR" in wangtype:
            wangtype = "V"
        model = "ADV-%s" % (wangtype)
    elif s[-7:] == "wang-muR":
        model = "ADV-V"
    if "_md_" in s:
        model = "MDi"
    return model


glob_handlers["model"] = modelHandler


def floatHandler(identifier, default, pprint=None):
    """
    Returns a function which handles identifier+float labels.
    The returned function returns the float value associated with the identifier as a string.
    If a pretty printer is given, it is used instead and the output is expected to be a string.
    Might not be the cleanest design, but it is easy to work with.
    """
    global glob_regexen, glob_handlers
    glob_regexen[identifier] = re.compile("_%s([-0-9.]+)_" % (identifier))

    def thisfunc(s, warn=False, default=default, pprint=pprint, nodefaultprint=True):
        m = glob_regexen[identifier].search(s)
        try:
            val = float(m.groups()[0])
        except AttributeError:  # no match found
            if warn:
                warnings.warn(
                    "no %s found in name %s, using default %.3g"
                    % (identifier, s, default)
                )
            val = default
        if val == default and nodefaultprint:
            return ""
        if pprint is not None:
            val = pprint(val)
        else:
            val = str(val)
        return val

    glob_handlers[identifier] = thisfunc
    return thisfunc


def pprint_except_default(pprint, default):
    def ifunc(x, default=default):
        if x != default:
            return pprint(x)
        else:
            return ""

    return ifunc


muHandler = floatHandler(
    "mu", default=0, pprint=lambda x: "p = %.3g MPa" % (x * Er0 / 1e6)
)


def pprint_tau(x):
    if abs(x - 1) < 1e-6:
        return r"$\tau_{gb} = \tau_{gv}$"
    else:
        return r"$\tau_{gb} = %.3g\tau_{gv}$" % (x)


taufHandler = floatHandler("tauf", default=100, pprint=pprint_tau)
# pprint=(lambda x: r"$\tau_{gb} = %.3g\tau_{gv}$" % (x)),

relaxfHandler = floatHandler(
    "relaxf",
    default=1,
    pprint=lambda x: (r"$t_{r} = %.4gt_{r,0}$" % (x)),
)

taugkfHandler = floatHandler(
    "taugkf",
    default=1,
    pprint=(lambda x: r"$\tau_{gv} = %.3g\tau_{gv,0}$" % (x)),
)

DgbfHandler = floatHandler(
    "Dgbf",
    default=1,
    pprint=(lambda x: r"$D_{gb} = %.3gD_{gb,0}$" % (x)),
)
DsfHandler = floatHandler(
    "Dsf",
    default=1,
    pprint=(lambda x: r"$D_{s} = %.3gD_{s,0}$" % (x)),
)

# muHandler = floatHandler(
#     "mu", default=0, pprint=lambda x: "p = %.3g MPa" % (x * Er0 / 1e6)
# )
# taufHandler = floatHandler(
#     "tauf",
#     default=100,
#     pprint=pprint_except_default(lambda x: r"$\tau_{gb} = %.3g\tau_{gv,0}$" % (x), 100),
# )

# relaxfHandler = floatHandler(
#     "relaxf",
#     default=1,
#     pprint=pprint_except_default(
#         lambda x: pprint_except_default(r"$t_{r} = %.3gt_{r,0}$" % (x), 1), 1
#     ),
# )

# taugkfHandler = floatHandler(
#     "taugkf",
#     default=1,
#     pprint=pprint_except_default(lambda x: r"$\tau_{gv} = %.3g\tau_{gv,0}$" % (x), 1),
# )

# DgbfHandler = floatHandler(
#     "Dgbf",
#     default=1,
#     pprint=pprint_except_default(lambda x: r"$D_{gb} = %.3gD_{gb,0}$" % (x), 1),
# )
# DsfHandler = floatHandler(
#     "Dsf",
#     default=1,
#     pprint=pprint_except_default(lambda x: r"$D_{s} = %.3gD_{s,0}$" % (x), 1),
# )


## all handlers should be added by now, so add a revdict for name lookup
glob_handlers_rev = {v: k for k, v in glob_handlers.items()}

# we have an API to read what-was-simulated in python, but that isn't open source, so we use the excellent filenames
def mkName(s, handlers=None, warn=False, nodefaultprint=True):
    if handlers == None:
        # avoid mutable default confusion
        handlers = [modelHandler, RHandler]
    name = ""
    for handler in handlers:
        try:
            name += handler(s, warn=warn, nodefaultprint=nodefaultprint) + " "
        except Exception as e:
            # TODO backtrace
            print("failed for handler %s, continuing" % (glob_handlers_rev[handler]))
            print(e)
            continue
    return " ".join(
        name.split()
    )  # remove trailing & leading whitespace, multi whitespace to single whitespace


def removeSurfaceGrains(graindata, graincoms, cutoff=32):
    """
    removes data related to grains on/near the surface

    on/near surface ~ 32nm of the bounding box via the COMs by default; use cutoff to vary this
    this assumes graindata starts with the shape (framecount,phasecount), so take care to pass it correctly
    since the com filter can vary with time, a different shape can result in each frame, so returned dtype will be object
    it's a inefficient not to cache the grainscoms filter, but suffices for now

    returns the appropriately filtered graindata
    """
    ret = []
    frames = graindata.shape[0]
    for frame in range(frames):
        icoms = graincoms[frame]
        # exclude missing grains + vapour: these have coms at 0
        ifilt = np.all(icoms > 0, axis=1)
        icoms_filt = icoms[ifilt]
        mins, maxs = np.min(icoms_filt, axis=0), np.max(icoms_filt, axis=0)
        minpos = mins + cutoff
        maxpos = maxs - cutoff
        comfilt = np.all(icoms > minpos, axis=1) & np.all(icoms < maxpos, axis=1)
        ret.append(graindata[frame, comfilt])
    ret = np.array(ret, dtype=object)
    return ret


def youngsLaw(gab_grain, gab_vap):
    return 2 * np.arccos(gab_grain / (2 * gab_vap))


# neck length / grain boundary length divided by the initial particle radius
def gbLengthLange(theta):
    """neck length / grain boundary length divided by the initial particle radius"""
    return (
        (1 / 2)
        * (
            np.sqrt(np.pi)
            * (theta + np.sin(theta) - np.pi)
            * np.sqrt(1 / (-theta + np.sin(theta) + np.pi))
            + np.sqrt(np.pi) / np.sqrt(1 / (-theta + np.sin(theta) + np.pi))
        )
        / np.cos((1 / 2) * theta)
    )


def shrinkageLange(theta):
    return (
        -np.sqrt(np.pi)
        * np.sqrt(1 / (-theta + np.sin(theta) + np.pi))
        * np.cos((1 / 2) * theta)
        + 1
    )

X, Y, Z = 0, 1, 2
dirs = np.array([X, Y, Z])
dirnames = ["X", "Y", "Z"]
def calcStrain(coms):
    first = coms[0]
    ulist = []
    poslist = []
    #1d slices
    for frame in range(1,coms.shape[0]):
        nonzero = coms
        curcom = coms[frame]
        nonzerofilt = np.any(curcom != 0, axis=1)
        comsF = first[nonzerofilt]
        disp = (comsF-coms[frame][nonzerofilt])

        minen, maxen = [], []
        for testdir in [0,1,2]:
            minen.append(np.min(comsF[:,testdir]))
            maxen.append(np.max(comsF[:,testdir]))
        bincounts = 6 #[12, 12, 12]
        # -+ so we cut off the edges, roughly

        binedges = []
        binpos = []
        for testdir in [0,1,2]:
            binedges.append(np.linspace(minen[testdir]+12, maxen[testdir]-12, bincounts))
            binpos.append(np.zeros(bincounts-1))
        #np.linspace(xmin+12, xmax-12, bincount)
        #bins = []
        ubinned = np.zeros((3, bincounts-1))

        for testdir in [X,Y,Z]:
            filtcount = 0
            for dircount in range(bincounts-1):
                mins = binedges[testdir][dircount]
                maxs = binedges[testdir][dircount+1]
                filt = (comsF[:,testdir] > mins) & (comsF[:,testdir] < maxs)
                binpos[testdir][dircount] = 0.5*(mins + maxs)

                if np.any(filt):
                    umeans = np.average(disp[filt,testdir])
                    ubinned[testdir,dircount] = umeans

                else:
                    ubinned[testdir,dircount] = np.nan
                #if testdir == X:
                #    filtcount += np.count_nonzero(filt)
            #if testdir == X:
            #    tmpr = rs[frame]
            #    rfilt = tmpr > 0
            #    avgr = np.average(tmpr[rfilt], weights = vols[frame][rfilt])
            #    #print(filtcount, times[frame], avgr)

        ulist.append(ubinned)
        poslist.append(binpos)
    epsen = [[0,0,0]] # initial condition are unstrained 
    epsen_std = [[np.array((0,0,0)), np.array((0,0,0))]]
    for pos, u in zip(poslist, ulist):
        divu = []
        for tdir in [X, Y, Z]:
            divu.append(np.array(np.gradient(u[tdir], pos[tdir] )))
        divu = np.array(divu)
        avgeps = np.average(divu, axis=1)
        epsen.append(avgeps)

        mineps, maxeps = avgeps-np.min(divu, axis=1), np.max(divu, axis=1)-avgeps # for errorbars
        epsen_std.append([mineps, maxeps])

    epsen = np.array(epsen)
    epsen_std = np.array(epsen_std)
    return epsen, epsen_std, ulist, poslist


# future: use parts of names as input and generate keys from the output of handlers

models = ["DO", "ADV-$\mu$", "MDi"]
sizes = [2.5, 5, 10, 20, 40]
# coloursen = ["green", "blue", "orange", "purple"]
# coloursen = ["green", "orange", "blue", "purple", "teal", "brown"]
coloursen = [
    "green",
    "blue",
    "orange",
    "purple",
    "brown",
    "teal",
]  # keep teal at the back for base
lsen = ["o", "x", "v", "s", "*"]
styles = {k: v for k, v in zip(sizes, lsen)}

colours = {k: v for k, v in zip(models, coloursen)}

sizes_packs = [8, 12, 16]
sizes_packs = ["R = %.2g nm" % (physSizeNm(R * 0.1)) for R in sizes_packs]
coloursR = {k: v for k, v in zip(sizes_packs, coloursen)}

modlsen2 = ["-", "--", "-.", "--."]
modlsen = {k: v for k, v in zip(models, modlsen2)}

# r"$%d^3$" % (baselen)
modlsen2 = ["-o", "-x", "-v", "--."]
packs = [200, 400, 800]
packs = [r"$%d^3$" % (x) for x in packs]
ls_pack = {k: v for k, v in zip(packs, modlsen2)}

# packs = [200, 400, 800]
# packs = [r"$%d^3$" % (x) for x in packs]
# ls_pack = {k: v for k, v in zip(packs, modlsen2)}
