*Supplementary Material to "Revealing process and material parameter effects on densification"*

The two notebooks treat the main analysis of pressure and mobility variation during sintering and creep simulations separately.

The directory dispmodel implements the velocity/displacement calculation model for a structure with a gradient in grain size, showing differential densification.

Alternative repo with more description:
[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8263533.svg)](https://doi.org/10.5281/zenodo.8263533)


Binder for interactive trialling:
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gh/https%3A%2F%2Fgitlab.kit.edu%2Fmarco.seiz%2Fprocess-mat-influence-sintering/HEAD)


