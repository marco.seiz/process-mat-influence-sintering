# helper functions for the supplementary material of
# Unravelling densification during sintering by multiscale modelling of grain motion
# by Seiz et al., henceforth referred to as "the paper"
# other paper references:
# Wang 2006: Computer modeling and simulation of solid-state sintering: A phase-field approach
# Seiz 2023: An improved grand-potential phase-field model of solid-state sintering for many particles


import numpy as np
import scipy.spatial as spatial
import scipy.sparse as sparse
import struct

import networkx as nx

# contact detection
# 1x kdtree, plus brute-force
def getContacts_kd(pos, rad, varrad=False):
    """
    Determines contacts between spheres/circles described by pos/rad.
    For variable radii, pass varrad=True, which will make the search O(n^2) in the worst case but usually only O(n).

    Returns the pruned contact list + the generated kdtree.
    """
    # assume pos is the wrong way around, thus swap
    ppos = np.swapaxes(pos, 0, 1)
    # use a kdtree for contact search within a 2*r sphere
    kd = spatial.KDTree(ppos)
    if varrad:
        maxrad = np.max(rad)
    cs = []
    for totpos, radius in zip(ppos, rad):
        # this could work with var(r) by using a max(radius) first,
        # then confirming for hits to be within radius + rad[hits] of totpos, but that turns it into O(n)
        testrad = 2 * radius
        if varrad:
            testrad = 2 * maxrad

        tmp_hits = kd.query_ball_point(totpos, testrad)
        if varrad:
            true_hits = []
            for hit in tmp_hits:
                exrad = radius + rad[hit]
                dist = np.sum((totpos - ppos[hit]) ** 2)
                if dist <= exrad**2:
                    true_hits.append(hit)
        else:
            true_hits = tmp_hits
        cs.append(true_hits.copy())
    for i, cont in enumerate(cs):
        # prune self-interaction + interaction with lower numbers
        js = []
        for j in cont:
            if j <= i:
                js.append(j)
        for j in js:
            cont.remove(j)
    return cs, kd


def getContacts(pos, rad, nump, contacts):
    """
    Determines the contacts between spheres/circles described by pos/rad in the naive O(n^2) way.

    Returns the contact list.
    """
    for p in range(nump):
        for pp in range(p + 1, nump):
            if True:  # p != pp:
                dx = pos[0][p] - pos[0][pp]
                dy = pos[1][p] - pos[1][pp]
                dz = pos[2][p] - pos[2][pp]
                dist = dx * dx + dy * dy + dz * dz
                # if (dist < rad[p]**2):
                if dist < (rad[p] + rad[pp]) ** 2:
                    contacts[p].append(pp)
    return contacts


def getGBcount(contacts):
    """
    This should also be available from the graph, but the list approach works for now(TM).

    Returns the number of contacts == grain boundaries in the system based on the contact list.
    """
    gbcount = 0
    for contact in contacts:
        gbcount += len(contact)
    return gbcount


def testConnectivity(contacts):
    """
    Tests whether the generated packing is fully connected.

    Returns True if connected, else False.
    """

    G = nx.Graph()
    for i, c in enumerate(contacts):
        G.add_node(i)
        for cc in c:
            G.add_edge(i, cc)
    return nx.is_connected(G)


# simple packers for test cases
# input are just the to-be-written to arrays

# can rotate chain direction by rot
def fill2DChain(pos, rad, numx, numy=1, theta=0):
    """
    Fill a 1D/2D chain of circles, which can be rotated by some angle theta.
    The input arrays pos, rad and size will be modified in-place.
    """
    rot = np.array([[np.cos(theta), -np.sin(theta)], [np.sin(theta), np.cos(theta)]])
    posamp = 1
    for py in range(numy):
        for p in range(numx):
            val = 0  # posamp*(1-2*np.random.rand()) # add noise to positions
            pp = p + py * numx
            vec = np.array([p, py])
            vec = np.dot(rot, vec)
            pos[0][pp] = vec[0]
            pos[1][pp] = vec[1]
            rad[
                pp
            ] = 0.51  # .55 is for normal direct contact packing, .75 for a bit more


def fillCircularChain(pos, rad, num, Rtot=1):
    """
    Fill a circular chain of grains.
    Rtot is the radius of the circle along which the grain COMs will lie.
    I suspect that this cannot be solved uniquely since the chain is closed.
    An open-ended chain should be solvable because it's topologically equivalent to the linear chain.

    The input arrays pos, rad and size will be modified in-place.
    """

    posamp = 1
    itheta = (2 * np.pi) / num
    r = (
        1.1 * np.sqrt(2 * (1 - np.cos(itheta))) * Rtot / 2
    )  # tangent circles from geometry + safety factor

    thetas = itheta * np.arange(0, num)
    xen, yen = Rtot * np.cos(thetas), Rtot * np.sin(thetas)
    rad[:] = r
    pos[0, :] = xen
    pos[1, :] = yen


def fillSpiralChain(pos, rad, num, maxtheta=4 * np.pi, ratio=(1 + np.sqrt(5)) / 2):
    """
    Fill a logarithmic spiral chain of grains.
    maxtheta is the maximum turning angle in radian.
    ratio is how much the radius grows for every right angle turn.

    NB that the fit solution fails to describe this because the
    grain boundaries are not distributed homogeneously through the spiral.
    Also, the conservation of momentum needs to be amended to read something like

    u -= np.average(u * rad**dim)/np.sum(rad**dim)

    to account for the variable size.

    It should be solvable exactly because it is open-ended.


    The input arrays pos, rad and size will be modified in-place.
    """

    b = np.log(ratio) / (np.pi / 2)
    itheta = (maxtheta) / num
    thetas = itheta * np.arange(-1, num)  # backwards one step for ease of coding
    Rs = 1 * np.exp(b * thetas)  # COM positions

    rs = (
        1.1
        * np.sqrt(Rs[:-1] ** 2 + Rs[1:] ** 2 - 2 * Rs[:-1] * Rs[1:] * np.cos(itheta))
        / 2
    )
    # same as for circular chain, but account for different side lengths
    xen, yen = Rs[1:] * np.cos(thetas[1:]), Rs[1:] * np.sin(thetas[1:])
    rad[:] = rs[:]
    pos[0, :] = xen
    pos[1, :] = yen


# random circle packing
# uniformly distributed in the interval [-posamp, +posamp]
# note that this can produce disconnected packings which aren't actually handled
def fillRandomCircles(pos, rad, nump, posamp=1.1, posmid=0):
    """
    Fills random circles in a 2D domain, no guarantee as for connectivity.
    pos and rad will be modified in-place.
    """
    randvals = 1 - 2 * np.random.rand(2, nump)
    pos[0, :] = randvals[0, :] * posamp - posmid
    pos[1, :] = randvals[1, :] * posamp - posmid
    rad[:] = np.sqrt(
        (2 * posamp) ** 2 / nump / np.pi
    )  # 100% density in the area, but doesn't guarantee connectivity


# ensures connectedness by placing circles in touching order, basically a random walk, with the radius being kept such that it touches the previous circle
# note circles will thus tend to bunch around the origin and lots of "collisions" happen
def fillRandomCircles_connected(pos, rad, nump):
    """
    Fills random circles in a 2D domain, while ensuring connectivity by limiting the distance between sequential circles.
    This will tend to generate lots of contacts since circles will generally overlap strongly.
    pos and rad will be modified in-place.
    """
    randvals = 1 - 2 * np.random.rand(2, nump)
    xdist = randvals[0, 1:] - randvals[0, :-1]
    ydist = randvals[1, 1:] - randvals[1, :-1]
    norms = np.sqrt(xdist**2 + ydist**2)
    xdist /= norms
    ydist /= norms  # ensure dist == 1
    pos[0, 0] = randvals[0, 0]  # * posamp - posmid
    pos[1, 0] = randvals[1, 0]  # * posamp - posmid
    rad[:] = 0.51  # since dist == 1, this ensures some overlap
    for p in range(1, nump):
        pos[0, p], pos[1, p] = (
            pos[0, p - 1] + xdist[p - 1],
            pos[1, p - 1] + ydist[p - 1],
        )


def readFromDatafile(path, dim=3):
    """
    Reads in spheres from a binary file.
    These are formatted as XYZR with a f32 for each value.
    Returns the resulting position and radius arrays.
    """
    rad = []
    pos = []
    for d in range(dim):
        pos.append([])
    with open(path, mode="rb") as f:
        while True:
            buf = f.read(16)
            if not buf:
                break
            x, y, z, r = struct.unpack("<ffff", buf)
            pos[0].append(x)
            pos[1].append(y)
            pos[2].append(z)
            rad.append(r * 1.3)  # ensures actual connection, not done for the PF sims
    return np.array(pos), np.array(rad)


def ini_pos(pos, scalex=1, scaley=1):
    """
    Builds initial guess simply based on particle position.
    Slightly off from the actual midpoint so the iterative algorithms have something to work with.

    Returns an initial guess for the displacements based on the particle positions.
    """
    return 0.95 * np.average(pos[0], axis=0) - pos


def nearestNeighboursOnly(contacts, pos, rhsmid=1, rhsspread=0, posdep=None):
    """
    Calculates the particle displacement by summing up only nearest-neighbour interactions.

    This is similar to Wang's 2006 paper and the resulting
    displacement profiles fit quite well with the velocity profiles
    obtained in 3D structures in Seiz's 2023 paper.

    Returns the particle displacement in the same shape as pos.
    """
    if posdep == None:  # avoid mutable default argument troubles
        posdep = lambda x: 0

    dim, nump = pos.shape
    sol = np.zeros_like(pos)
    for p in range(nump):
        for gbp in contacts[p]:
            rnd = np.random.normal(rhsmid, rhsspread)
            rnd += posdep(pos[0, p])  # bias by 0th position = x
            gbtot = pos[:, p] - pos[:, gbp]
            norm = np.linalg.norm(gbtot, 2)
            gbtot /= norm
            for d in range(dim):
                gbn = gbtot[d]
                sol[d, p] += -rnd * np.sign(gbn)
                sol[d, gbp] += rnd * np.sign(gbn)
    return sol


def buildSystem(pos, rad, contacts, rhsmid=1.0, rhsspread=0.25, posdep=None):
    """
    Builds the system Cu=\Delta u as described in the paper.
    \Delta u = rhs is assumed to be a normal distribution, with the direction given by the grain boundary normal.
    rhsmid, rhsspread are the position (mean) and scale (stddev) of the normal distribution, cf. the numpy docs.
    posdep allows passing a single-argument function to give a spatial bias to the rhs; by default there is none.
    Its argument is assumed to be, for simplicity, only the x position of the particle

    Returns the system matrix C as a list of sparse matrices per dimension and a list of rhs per dimension.

    """
    dim, nump = pos.shape
    dokmats = []
    gbcounter = 0
    gbcount = getGBcount(contacts)
    rhsen = np.zeros((dim, gbcount))
    rhsscale = 1  # arbitrary number which scales the random distribution

    if posdep == None:  # avoid mutable default argument troubles
        posdep = lambda x: 0

    for d in range(dim):
        dokmats.append(sparse.dok_matrix((gbcount, 1 * nump)))
    for p in range(nump):
        for gbp in contacts[p]:
            # we assume that the grain boundary normal is given by the connecting vector of the COMs
            # this is just for the notebook, whereas in the PF code the grain boundary normal
            # is found by averaging grad(phia)-grad(phib) over each ab-interface
            gbtot = pos[:, p] - pos[:, gbp]
            norm = np.linalg.norm(gbtot, 2)
            gbtot /= norm

            rnd = np.random.normal(rhsmid, rhsspread)
            # a bias by the position *will* cause the fit solution to deviate, as it implies
            # a nonlinear profile
            rnd += posdep(pos[0, p])  # bias by 0th position = x
            for d in range(dim):
                gbn = gbtot[d]
                dokmats[d][gbcounter, p] = -1 * np.sign(gbn)
                dokmats[d][gbcounter, gbp] = 1 * np.sign(gbn)

                rhsen[d][gbcounter] = rnd * rhsscale * np.abs(gbtot[d])
            gbcounter += 1
    cscmats = [x.tocsc() for x in dokmats]
    return cscmats, rhsen


# this is written similar to the actual implementation in the PF code.
# this allows showcasing where communication is necessary
def fit_distributed(mat, rhs, pos, rad=None, dim=None, N=2):
    """
    Solves Mat.u = rhs assuming u = m*(x-x0), with x = pos, x0=np.average(pos) and the data being spread across N workers.
    Note that this doesn't take different grain sizes into account which the PF code does.

    Returns the per-grain displacements u == solution vector.
    """

    # distribute the array
    perproc = rhs.shape[0] // N
    lenlist = np.ones(N, dtype=np.int32) * int(perproc)
    if perproc != int(perproc):
        off = rhs.shape[0] % N
        for i in range(off):
            lenlist[i] += 1
    rangelist = np.cumsum(lenlist)
    rangelist = np.insert(rangelist, 0, 0)
    perproc = int(perproc)

    # roughly from now on is the per-timestep code, excluding necessary communication prior
    # + the matrix/rhs building
    if rad is not None:
        # dim should be passed as well, otherwise you'll get an exception
        x0 = np.average(pos * rad**dim) / np.sum(
            rad**dim
        )  # estimate as d-dimensional cube, should be reasonable
    else:
        x0 = np.average(pos)  # should be volume-weighted and is in the PF code

    diffvec = pos - x0

    sum_subp, sum_subq = 0.0, 0.0
    # each "worker" calculates its own part of (p, q) which get "reduced" at the end of the loop
    for i in range(N):
        st = rangelist[i]
        end = rangelist[i + 1]
        submat = mat[st:end]
        subrhs = rhs[st:end]
        # work in parallel is basically 1x spmv + 2 dot products
        # print(1, submat.shape, (diffvec.shape))
        M = submat @ diffvec  # (diffvec[st:end])
        subq = np.dot(M, M)
        subp = np.dot(M, subrhs)
        # "reduce"
        sum_subp += subp
        sum_subq += subq
    # as a side note, the results will differ slightly by the number of workers
    # due to accumulated roundoff errors. if MPI provided a performant Kahan summation reduction it would be neat.

    m = sum_subp / sum_subq

    return m * (pos - x0)
